import { CollectionObject } from './collection-object.model';

export interface Party extends CollectionObject {
  name: string;
  owner?: string;
  public: boolean;
}
