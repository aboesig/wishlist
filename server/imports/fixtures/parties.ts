import { Parties } from '../../../both/collections/parties.collection';
import { Party } from '../../../both/models/party.model';

export function loadParties() {
  if (Parties.find().cursor.count() === 0) {
    const parties: Party[] = [{
      name: 'Dubstep-Free Zone',
      public: true
    }, {
      name: 'All dubstep all the time',
      public: true
    }, {
      name: 'Savage lounging',
      public: false
    }];

    parties.forEach((party: Party) => Parties.insert(party));
  }
}