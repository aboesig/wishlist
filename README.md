# Wishlist - A Meteor-Angular 2 App

##Made by Andreas Boesig - November/December 2016

Make users and add wishes to the list. Choose between public or hidden wishes.

##Link to project

https://aboesig@bitbucket.org/aboesig/wishlist-angular-2.git

# Start the project

- ´$ npm install´ // Installs all project dependencies.
- ´$ meteor´ // Starts the project and the database.
